﻿using System;
using System.IO;
using System.Threading;

namespace OriSaveBackuper
{
    class Program
    {
        static void Main(string[] args)
        {
            Searcher searcher = new Searcher(@"C:\Users\Andrii\AppData\Local\Ori and the Blind Forest DE", "saveFile0.sav");
            searcher.Run();
            Console.ReadKey();
        }
    }

    class Searcher
    {
        private const string saveFilePathName = @"C:\Users\Andrii\AppData\Local\Ori and the Blind Forest DE\saveFile0.sav";
        private const string backupFile0PathName = @"E:\Backup Ori\saveFile0.sav";
        private const string backupFile1PathName = @"E:\Backup Ori\saveFile1.sav";

        public int Counter { get; set; }

        public FileInfo InfoSave { get; set; }

        private readonly FileSystemWatcher fsw;

        public Searcher(string path, string filter)
        {
            InfoSave = new FileInfo(saveFilePathName);

            Counter = 0;

            fsw = new FileSystemWatcher(path, filter);
            fsw.Changed += new FileSystemEventHandler(fsw_Changed);
        }
        public void Run()
        {
            fsw.EnableRaisingEvents = true;
        }
        void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                Thread.Sleep(100);
                InfoSave = new FileInfo(saveFilePathName);
                if (InfoSave.Length > 20000)
                {
                    Console.WriteLine($"{++Counter}. Произошло сохранение, производится бэкап файла.");
                    File.Copy(backupFile0PathName, backupFile1PathName, true);
                    File.Copy(saveFilePathName, backupFile0PathName, true);
                    fsw.EnableRaisingEvents = false;
                }
                else
                {
                    Console.WriteLine($"{++Counter}. Персонаж погиб, производится перезапись сохранения из бэкапа.");
                    File.Copy(backupFile1PathName, saveFilePathName, true);
                    File.Copy(backupFile1PathName, backupFile0PathName, true);
                    fsw.EnableRaisingEvents = false;
                }
            }

            finally
            {
                fsw.EnableRaisingEvents = true;
            }
        }
    }
}
